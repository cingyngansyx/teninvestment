import HeroSectionView from "../hero-section-view";
import AdvantagesView from "../advantages-view";
import OurServiceView from "../our-service-view";
import NewsfeedSectionView from "../newsfeed-view";
import InformationView from "../information-view";
import FooterView from "../footer-view";
import ReactPageScroller from "react-page-scroller";

function IntroductionMainView() {

    return (
        <div className="flex flex-col bg-[#F3F8FD]">
            <ReactPageScroller
            // pageOnChange={this.handlePageChange}
            // onBeforePageScroll={this.handleBeforePageChange}
            // customPageNumber={this.state.currentPage}
            >
                <div className="pt-[110px]">
                    <HeroSectionView />
                </div>
                <div className="pt-[110px]">
                    <AdvantagesView />
                </div>
                <div className="pt-[110px]">
                    <OurServiceView />
                </div>
                <div className="pt-[110px]">
                    <NewsfeedSectionView />
                </div>
                <div className="pt-[110px]">
                    <InformationView />
                </div>
                <div className="mt-[110px]">
                    <FooterView />
                </div>
            </ReactPageScroller>
        </div>
    );
}

export default IntroductionMainView;
