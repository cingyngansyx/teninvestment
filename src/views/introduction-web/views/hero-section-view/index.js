import { ArrowRight, ArrowRight2, ArrowRight3 } from "iconsax-react";

function HeroSectionView() {

    return (
        <div className="grid grid-cols-5 gap-3 mt-10 px-[228px] text-neutral-500">
            <div className="col-span-3 text-7xl flex flex-wrap">
                Хөрөнгө оруулалтын хүчээр тогтвортой
                <div className="text-primary font-bold mr-2">ирээдүйг</div>
                бүтээнэ.
            </div>
            <div className="col-span-2 flex flex-col gap-3 font-thin text-justify">
                Бид үйлчлүүлэгчдийнхээ санхүүгийн хэрэгцээг хангахуйц ирээдүйд хурдацтай өсөн дэвших, үр ашиг өндөртэй үнэт цаас болон шинэ залуу төслүүдэд хөрөнгө оруулах зорилготойгоор хөрөнгө оруулалтын сангуудыг итгэмжлэн удирдах, зөвлөх үйлчилгээ үзүүлнэ.
                <div className="grid grid-cols-2">
                    <div className="flex items-center justify-between border-2 border-primary p-2 px-3 uppercase rounded-full font-bold">
                        Хөрөнгө оруулах
                        <ArrowRight className="p-1 bg-primary rounded-full text-white" />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default HeroSectionView;
