import { Call, Facebook, Location, MessageCircle } from "iconsax-react";
import WebIconWithTitle from "../../../../component/utils/IconTitle";


function FooterView(props) {

    return (
        <div className="flex flex-col px-[228px] py-[100px] gap-[48px] bg-[#202D3B] text-white">
            <div className="grid grid-cols-2 gap-[148px]">
                <div className="flex flex-col gap-[12px]">
                    <div>Бидэнтэй холбоо барих</div>
                    <div>Та цахим шуудангаа бидэнд илгээснээр бид танд сүүлийн үеийн мэдээ, мэдээллийг явуулах болно.</div>
                    <div className="grid grid-cols-6 gap-[24px]">
                        <div className="bg-[#ffffff14] rounded-[4px] text-[14px] py-[12px] px-[24px] w-full">И-мэйл хаягаа оруулна уу.</div>
                        <div className="rounded-[4px] bg-primary px-[64px] text-[20px] text-center py-[12px] px-[24px]">Илгээх</div>
                    </div>
                </div>
                <div className="grid grid-cols-2 gap-[148px]">
                    <div className="flex flex-col gap-[24px]">
                        <div className="text-[24px]">ХОЛБООС</div>
                        <div className="flex flex-col flex-1 gap-[12px]">
                            <div>Нүүр</div>
                            <div>Хөрөнгө оруулалтын сан</div>
                            <div>Бидний тухай</div>
                            <div>Холбоо барих</div>
                            <div>Нэвтрэх/Бүртгүүлэх</div>
                            <div>Эрсдэлийн удирдлага</div>
                        </div>
                    </div>
                    <div className="flex flex-col gap-[12px] text-[20px]">
                        <div className="flex flex-col gap-[6px]">
                            <div className="flex gap-[6px]">
                                <MessageCircle className="text-primary" variant="Bold" size={30} />
                                И-Мэйл:</div>
                            <div className="text-[16px] text-[#CAC5CD] text-[16px]">info@ten-investment.com</div>
                        </div>
                        <div className="flex flex-col gap-[6px]">
                            <div className="flex gap-[6px]">
                                <Call className="rotate-[-90deg] text-primary" variant="Bold" size={30} />
                                Утас:</div>
                            <div className="flex gap-[6px] px-[12px] text-[16px] text-[#CAC5CD] text-[16px]">
                                <div>+(976) 9910-7444</div>
                                <div>+(976) 9904-4501</div>
                            </div>
                        </div>
                        <div className="flex flex-col gap-[6px]">
                            <div className="flex gap-[6px]">
                                <Location className="text-primary" variant="Bold" size={30} />
                                Хаяг:</div>
                            <div className="text-[16px] text-[#CAC5CD] text-[16px] text-justify">Монгол улс, Улаанбаатар хот, Хан-Уул дүүрэг, 18-р хороо, Богд Жавзандамба гудамж, Эл Эс плаза, 401а тоот</div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="flex items-center justify-between border-t border-primary pt-[12px]">
                <div className="flex items-center gap-[48px]">
                    <WebIconWithTitle />
                    Copyright by 2019 Tenplus, Inc
                </div>
                <div className="grid grid-cols-4 gap-[24px]">
                    <Facebook />
                    <Facebook />
                    <Facebook />
                    <Facebook />
                </div>
            </div>
        </div>
    );
}

export default FooterView;
