

function AdvantageInfo(props) {

    return (
        <div className="flex gap-3">
            <div className="min-h-[88px] min-w-[88px]">
                <div className="h-[88px] w-[88px] rounded-md bg-blue-400"></div>
            </div>
            <div className="flex flex-col gap-3 text-neutral-600 text-[16px] text-justify">
                <div className="font-semibold text-[22px]">{props.title}</div>
                {props.info}
            </div>
        </div>
    );
}

export default AdvantageInfo;
