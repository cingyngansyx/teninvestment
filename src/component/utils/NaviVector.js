import { ArrowCircleRight } from "iconsax-react";
import { useEffect, useState } from "react";

function NaviVector(props) {
  const [position, setPosition] = useState(props.position);

  useEffect(() => {
    if (props.position) {
      setPosition(props.position);
    }
  }, [props.position]);

  return (
    <div
      className={"p-[5px] rounded-full bg-white rotate-" + position + ""}
    >
      <ArrowCircleRight variant="Bold" color="#00904D" />
    </div>
  );
}

export default NaviVector;
