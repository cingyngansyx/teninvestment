import { ArrowRight2 } from "iconsax-react";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function NavigationSubButton(props) {
  const [active, setActive] = useState();

  useEffect(() => {
    setActive(props.active);
  }, [props.active]);

  return (
    <div className="relative px-2.5 text-xs cursor-pointer border-l border-neutral-4 last:border-0">
      <Link to={props.path ? props.path : ""}>
        <div className="absolute w-2.5 h-1/2 top-0 -left-[1px] rounded-bl-full border-b border-l border-neutral-4"></div>
        <div
          className={
            "flex text-neutral-400 items-center gap-3 justify-between w-[180px] p-2 hover:text-neutral-600 rounded-lg leading-4 " +
            (active ? "!text-blue-500 font-bold" : "")
          }
        >
          <span>{props.children}</span>
          {active ? <ArrowRight2 size={"16px"} /> : ""}
        </div>
      </Link>
    </div>
  );
}

export default NavigationSubButton;
